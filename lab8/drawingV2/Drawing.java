package drawingV2;

import java.util.ArrayList;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();	
	
	public double calculateTotalArea() {
		double totalArea = 0;
		for (Object obj : shapes) {
			if (obj instanceof Circle) {
				Circle circle = (Circle) obj;
				totalArea += circle.area();
			}
			else if (obj instanceof Rectangle) {
				Rectangle rect = (Rectangle) obj;
				totalArea += rect.area();
			}
			else if (obj instanceof Square) {
				Square square = (Square) obj;
				totalArea += square.area();
			}
		}
		return totalArea;
	}
	
	public void addShape(Object object) {
		shapes.add(object);
	}
	
}

