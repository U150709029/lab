package drawingV3;

public class Test {

	public static void main(String[] args) {
		Drawing drawing = new Drawing();
		drawing.addShape(new Circle(5));
		drawing.addShape(new Rectangle(3,6));
		drawing.addShape(new Circle(9));
		drawing.addShape(new Rectangle(2,6));
		
		System.out.println(drawing.calculateTotalArea());

	}

}

