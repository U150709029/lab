package stack;

public class Test {
	public static void main(String[] args) {
        StackArrayListImpl<String> stackTest = new StackArrayListImpl<String>();
        stackTest.push("A");
        stackTest.push("B");
        StackImpl<String> stack =  new StackImpl<String>();		
		
		stack.push("Hello");
		stack.push("ABC");
		stack.push("My name is");
		stack.push("Selahattin");
		
		Object obj = stackTest.pop();
//		System.out.println(obj);
		
		System.out.println(stack.toList());
	}

}
