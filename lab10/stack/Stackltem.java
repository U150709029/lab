package stack;

public class StackItem<T> {
	T item;
	StackItem<T> previous;
	
	public StackItem(T item, StackItem<T> previous) {
		this.item = item;
		this.previous = previous;
	}
	
	public T getItem() {
		return item;
	}
	
	public StackItem<T> getPrevious() {
		return previous;
	}
}

