public class Circle{
  
    double radius;

    public Circle(double a){
  
	radius=a;

    }
    public double area(){
  
       return (radius*radius)*(3.14);

    }

    public double perimeter(){

       return 2*radius*3.14;
 

    }

}
